package com.cpe.asi.atelier1.service;

import com.cpe.asi.atelier1.dto.UtilisateurDto;

public interface UtilisateurService {
    public void ajouterUtilisateur(UtilisateurDto utilisateurDto);
}
