package com.cpe.asi.atelier1.constants;

public class Atelier1Constants {
    /* PAGE DU CRUD */
    public static final String AJOUTER = "/ajouter";
    public static final String SUPPRIMER = "/supprimer";
    public static final String MODIFIER = "/modifier";
    public static final String RECHERCHER = "/modifier";

    /* CHEMIN */
    public static final String INDEX = "/";
    public static final String REDIRECT = "redirect:";

    /* PAGE DES VUES */
    public static final String PAGE_CARD = "/card";
    public static final String PAGE_FETCH = "/fetch";
    public static final String PAGE_FORM_SAMPLE = "/form-sample";
    public static final String PAGE_SEARCH_CARD = "/searchCard";
    public static final String PAGE_CREER = "/creer";
}
