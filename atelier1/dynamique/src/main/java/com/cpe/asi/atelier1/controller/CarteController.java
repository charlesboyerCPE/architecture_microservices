package com.cpe.asi.atelier1.controller;

import com.cpe.asi.atelier1.constants.Atelier1Constants;
import com.cpe.asi.atelier1.dto.UtilisateurDto;
import org.slf4j.Logger;
import org.slf4j.Marker;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;


@Controller
public class CarteController {
    public static final String FORM_UTILISATEUR = "formUtilisateur";
    public static final String UTILISATEUR = "utilisateur";

    @GetMapping(value = Atelier1Constants.INDEX)
    public String displayAccueil() {
        return Atelier1Constants.PAGE_CARD;
    }

    @GetMapping(value = Atelier1Constants.AJOUTER)
    public String displayFormulaire(Model model) {
        model.addAttribute(FORM_UTILISATEUR, new UtilisateurDto());
        return Atelier1Constants.PAGE_CREER;
    }

    @PostMapping(value = Atelier1Constants.AJOUTER)
    public String ajouterInfoUtilisateur(@ModelAttribute(FORM_UTILISATEUR) @Validated UtilisateurDto formUtilisateurDto,
                                         final RedirectAttributes redirectAttributes) {

        if(formUtilisateurDto != null) {
            redirectAttributes.addFlashAttribute(UTILISATEUR, formUtilisateurDto);
        }

        return Atelier1Constants.REDIRECT + Atelier1Constants.INDEX;
    }
}
