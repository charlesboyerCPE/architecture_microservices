package com.cpe.asi.atelier1.dto;

public class UtilisateurDto {
    private String prenom;
    private String nom;
    private String imageUrl;
    private Boolean visibility;

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public Boolean getVisibility() {
        return visibility;
    }

    public void setVisibility(Boolean visibility) {
        this.visibility = visibility;
    }

    @Override
    public String toString() {
        return "UtilisateurDto{" +
                "prenom='" + prenom + '\'' +
                ", nom='" + nom + '\'' +
                ", imageUrl='" + imageUrl + '\'' +
                ", visibility=" + visibility +
                '}';
    }
}
